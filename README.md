# crystal framework

this is crystal php framework

more information: http://crystalphp.com

read documentation: https://gitlab.com/crystalphp/docs or https://github.com/crystalphp/docs

# create new project

using composer:

`$ composer create-project crystalphp/project projectname`
